const express = require('express');
const bodyParser = require('body-parser')
const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');
const mongoose = require('mongoose');
const path = require('path');
const multer = require('multer');


const app = express();
const MONGODB_URI = 'mongodb://root:secret@mongo:27017';

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().getTime() + '-' + file.originalname)
    }
})
const fileFilter = (req, file, cb) => {
    switch (file.mimetype) {
        case 'image/png':
        case 'image/jpg':
        case 'image/jpeg':
            cb(null, true);
            break
        default:
            cb(null, false);
    }
}
app.use(multer({storage: fileStorage, fileFilter: fileFilter}).single('image'))

// for form use x-www-form-urlencoded
// for api use json
app.use(bodyParser.json());

app.use('/images', express.static(path.join(__dirname, 'images')));
//set CORS headers
app.use((req, res, next) => {
    // or for all you can set in value *
    // tell which domains can accept
    res.setHeader('Access-Control-Allow-Origin', '*');
    // tell which methods can accept
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
})

app.use('/feed', feedRoutes);
app.use('/auth', authRoutes);

// handle errors
app.use((err, req, res, next) => {
    console.log(err);
    // in case is undefined set it to 500
    const status = err.statusCode || 500;
    const message = err.message;
    const data = err.data;
    res.status(status).json({
        message: message,
        data: data,
    })
})

mongoose.connect(MONGODB_URI, {
    dbName: 'messages'
})
    .then(res => {
        console.log('Connected to Mongoose');
        // first listen for the http requests then open another protocol
        const server = app.listen(8080);
        // set socket.io
        const io = require('./socket').init(server);
        // set event-listeners
        io.on("connection", socket => {
            console.log('Client connected');
        })
    })
    .catch(err => {
        console.log(err);
    })
