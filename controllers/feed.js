const { validationResult } = require('express-validator/check')
const fs = require('fs');
const path = require('path');
const Post = require('../models/post');
const User = require('../models/user');
const io = require('../socket');

exports.getPosts = async (req, res, next) => {
    const currentPage = req.query.page || 1;
    const perPage = req.query.perPage || 2

    try {
        const total = await Post.find().countDocuments();
        const posts = await Post.find({})
            .populate('creator', ['name'])
            .sort('-createdAt')
            .skip((currentPage - 1) * perPage)
            .limit(perPage);

        res.status(200).json({
            success: true,
            posts: posts,
            totalItems: total,
        })
    } catch (err) {
        if (! err.statusCode) {
            err.statusCode = 500
        }
        next(err);
    }
};

exports.getPost = (req, res, next) => {
    const postId = req.params.postId;
    Post.findById(postId)
        .then(post => {
            if (! post) {
                const err = new Error('Post not found')
                err.statusCode = 404;
                // this will got to catch
                throw err;
            }
            return res.status(200).json({
                success: true,
                post: post,
            });
        })
        .catch(err => {
            if (! err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
}

exports.postPosts = (req, res, next) => {
    const errors = validationResult(req);
    if(! errors.isEmpty()) {
        const error = new Error('Error Validation');
        error.statusCode = 422;
        throw error;
    }

    if(! req.file) {
        const error = new Error('No image provided');
        error.statusCode = 422;
        throw error;
    }

    const imageUrl = req.file.path;
    const title = req.body.title;
    const content = req.body.content;
    let creator;
    // create post in db
    const post = new Post({
        title: title,
        content: content,
        imageUrl: imageUrl,
        creator: req.userId
    })

    return post.save()
        .then(result => {
            return User.findById(req.userId);
        })
        .then(user => {
            if (! user) {
                const error = new Error('User not found');
                error.statusCode = 422;
                throw error;
            }
            creator = user;
            user.posts.push(post);
            return user.save();
        })
        .then(result => {
            // define event name "posts"
            io.getIO().emit('posts', {
                action: 'create',
                post: {
                    ...post._doc,
                    creator: {
                        _id: req.userId,
                        name: creator.name
                    }
                }
            })
            return res.status(201).json({
                success: true,
                post: post,
                creator: {_id: creator._id, name: creator.name}
            })
        })
        .catch(err => {
            if (! err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}

exports.updatePost = (req, res, next) => {
    const errors  = validationResult(req);
    const postId  = req.params.postId;
    const title   = req.body.title;
    const content = req.body.content;
    let imageUrl  = req.body.image;

    if (! errors.isEmpty()) {
        const error = new Error('Error Validation');
        error.statusCode = 422;
        throw error;
    }

    if (req.file) {
        imageUrl = req.file.path;
    }

    if(! imageUrl) {
        const error = new Error('No file picked');
        error.statusCode = 422;
        throw error;
    }

    Post.findById(postId)
        .populate('creator')
        .then(post => {
            if (! post) {
                const error = new Error('Could Not Find Post');
                error.statusCode = 404;
                throw error;
            }

            if (post.creator._id.toString() !== req.userId) {
                const error = new Error('You can not update this post. Unauthorized');
                error.statusCode = 403;
                throw error;
            }

            // delete file from server
            if (post.imageUrl !== imageUrl && imageUrl !== undefined) {
                clearImage(post.imageUrl);
                post.imageUrl = imageUrl;
            }
            // update
            post.title = title;
            post.content = content;
            return post.save();
        })
        .then(result => {
            io.getIO().emit('posts', {action: 'update', post: result})
            res.status(200).json({message: 'Post updated!', post: result})
        })
        .catch(err => {
            if (! err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}

exports.deletePost = (req, res, next) => {
    const postId = req.params.postId;
    Post.findById(postId)
        .then(post => {
            if (! post) {
                const error = new Error('Could not find post');
                error.statusCode = 404;
                throw error;
            }
            // check logged user
            if (post.creator.toString() !== req.userId) {
                const error = new Error('You can not update this post. Unauthorized');
                error.statusCode = 403;
                throw error;
            }
            // delete image
            clearImage(post.imageUrl);
           return Post.findByIdAndRemove(postId);
        })
        .then(result => {
           return User.findById(req.userId);
        })
        .then(user => {
            user.posts.pull(postId);
            return user.save();
        })
        .then(result => {
            io.getIO().emit('posts', {
                action: 'delete',
                post: postId
            })
            res.status(200).json({message: 'Post deleted'});
        })
        .catch(err => {
            if (! err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}

const clearImage = filePath => {
    filePath = path.join(__dirname, '..', filePath);
    fs.unlink(filePath, err => console.log(err));
}